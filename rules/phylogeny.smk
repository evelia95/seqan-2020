rule prokka:
	input:
		fa="velvet_output/{sample}/contigs.fa" if config["run_de_novo"] else "fasta/{sample}.fasta",
		t="../miniconda3/envs/prokka/bin/tbl2asn"
	output:
		"annotations/{sample}/prokka_{sample}.gff"
	conda:
		"../envs/prokka.yaml"
	params:
		genus = config["genus"],
		species = config["species"]
	threads: 8
	shell:
		"""
		prokka --outdir annotations/{wildcards.sample} --usegenus --genus {params.genus} --species {params.species} --prefix prokka_{wildcards.sample} {input.fa}
		"""
rule roary:
	input:
		expand("annotations/{sample}/prokka_{sample}.gff", sample=samples.index.values)
	output:
		"core_gene_alignment.aln"
	conda:
		"../envs/phylogeny.yaml"
	threads: 8
	shell:
		"roary -e --mafft -p 8 -cd 100.0 -i 99 {input}"

rule fasttree:
	input:
		"core_gene_alignment.aln"
	output:
		"fasttree.newick"
	resources:
		mem_mb=50000
	conda:
		"../envs/phylogeny.yaml"
	shell:
		"FastTree -gtr -nt {input} > {output}"
