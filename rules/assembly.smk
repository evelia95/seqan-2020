rule trim_reads:
	input:
		f1 = r1,
		f2 = r2
	output:
		out1P = "trimmed/{sample}_fq1P.fastq.gz",
		out1U = "trimmed/{sample}_fq1U.fastq.gz",
		out2P = "trimmed/{sample}_fq2P.fastq.gz",
		out2U = "trimmed/{sample}_fq2U.fastq.gz"
	conda:
		"../envs/qc.yaml"
	threads: 8
	params:
		adapters = config["adapters"]
	shell:
		"""
		trimmomatic PE -threads 4 {input.f1} {input.f2} {output.out1P} {output.out1U} {output.out2P} {output.out2U}  ILLUMINACLIP:{params.adapters}:2:30:1 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:25
		"""

rule build_index:
	input:
		[]
	output:
		"genomeindex.1.bt2"
	conda:
		"../envs/bowtie.yaml"
	params: 
		reference = config["reference"]
	shell:
		"bowtie2-build {params.reference} genomeindex"

rule map_reads:
	input:
		E="trimmed/{sample}_fq1P.fastq.gz",
		T="trimmed/{sample}_fq2P.fastq.gz",
		ind="genomeindex.1.bt2"
	output:
		"bam/{sample}.bam"
	conda:
		"../envs/bowtie.yaml"
	shell:
		"bowtie2 -x genomeindex -1 {input.E} -2 {input.T} | samtools view -S -b - > {output}"

rule bam_to_fasta:
	input:
		"bam/{sample}.bam"
	output:
		"fasta/{sample}.fasta"
	conda:
		"../envs/bowtie.yaml"
	shell:
		"samtools bam2fq {input} | seqtk seq -A > {output}"

rule velvet_hashing:
	input:
		E="trimmed/{sample}_fq1P.fastq.gz",
		T="trimmed/{sample}_fq2P.fastq.gz"
	output:
		"velvet_output/{sample}/Roadmaps"
	params:
		k=config["kmer_length"]
	conda:
		"../envs/velvet.yaml"
	shell:
		"velveth velvet_output/{wildcards.sample} {params.k} -shortPaired -separate -fastq {input.E} {input.T}"

rule velvet_assembly:
	input:
		"velvet_output/{sample}/Roadmaps"
	output:
		"velvet_output/{sample}/contigs.fa"
	params:
		len=config["read_length"],
		exp=config["expected_coverage"],
		cut=config["coverage_cutoff"]
	conda:
		"../envs/velvet.yaml"
	shell:
		"""
		velvetg velvet_output/{wildcards.sample} -ins_length {params.len} -exp_cov {params.exp}
		velvetg velvet_output/{wildcards.sample} -cov_cutoff {params.cut} -min_contig_lgth 500
		"""
