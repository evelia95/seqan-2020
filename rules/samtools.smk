rule sort_bam:
	input:
		"bam/{sample}.bam"
	output:
		"bam_sorted/{sample}_sorted.bam"
	conda:
		"../envs/samtools.yaml"
	resources:
		mem = 1000
	threads: 4
	shell:
		"samtools sort -o {output} {input}"

rule index_bam:
	input:
		"bam_sorted/{sample}_sorted.bam"
	output:
		"bam_sorted/{sample}_sorted.bam.bai"
	conda:
		"../envs/samtools.yaml"
	resources:
		mem = 1000
	threads: 4
	shell:
		"samtools index {input}"

rule stats:
	input:
		bam="bam_sorted/{sample}_sorted.bam",
		bai="bam_sorted/{sample}_sorted.bam.bai"
	output:
		"qc/{sample}_idxstat.txt"
	conda:
		"../envs/samtools.yaml"
	resources:
		mem = 1000
	threads: 4
	shell:
		"samtools idxstats {input.bam} > {output}"
