rule mlst:
	input:
		expand("velvet_output/{sample}/contigs.fa", sample=samples.index.values) if config["run_de_novo"] else expand("fasta/{sample}.fasta", sample=samples.index.values)
	output:
		"mlst/mlst.tsv"
	conda:
		"../envs/mlst.yaml"
	shell:
		"mlst {input} > {output}"	
