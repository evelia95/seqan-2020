rule raw_qc:
	input:
		r
	output: 
		html = "qc/{sample}_fq{run}_fastqc.html",
		zip = "qc/{sample}_fq{run}_fastqc.zip"
	conda:
		"../envs/qc.yaml"
	params: ""
	threads: 4
	wrapper:
		"0.27.0/bio/fastqc"

rule trimmed_qc:
	input:
		"trimmed/{sample}_fq{run}P.fastq.gz"
	output:
		html = "qc/{sample}_trimmed_fq{run}P_fastqc.html",
		zip = "qc/{sample}_trimmed_fq{run}P_fastqc.zip"
	conda:
		"../envs/qc.yaml"
	threads: 4
	wrapper:
		"0.27.0/bio/fastqc"

rule mapping_stats:
	input:
		"bam_sorted/{sample}_sorted.bam"
	output:
		"qc/{sample}_bamqc/genome_results.txt"
	params:
		outdir = "qc/{sample}_bamqc"
	conda:
		"../envs/qc.yaml"
	threads: 4
	shell:
		"qualimap bamqc -bam {input} -outdir {params.outdir} -nt 4"

rule multiqc:
	input:
		fastqc_raw = expand("qc/{sample}_fq{run}_fastqc.{type}", type=config["report_types"], sample=samples.index.values, run=run),
		fastqc_trimmed = expand("qc/{sample}_trimmed_fq{run}P_fastqc.{type}", type=config["report_types"], sample=samples.index.values, run=run),
		idxstats = [] if config["run_de_novo"] else expand("qc/{sample}_idxstat.txt", sample=samples.index.values),
		bamqc = [] if config["run_de_novo"] else expand("qc/{sample}_bamqc/genome_results.txt", sample=samples.index.values, run=run)
	output:
		"report/multiqc_report.html"
	shell:
		"""
		pip install multiqc
		multiqc qc/ -o report
		"""
