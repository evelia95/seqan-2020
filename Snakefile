#load  samples  into  table
import  pandas  as pd
import os

configfile: "config.yaml"
samples = pd.read_csv(config["samples"], index_col="sample", sep="\t")

#wildcard constraints to avoid ambiguity
wildcard_constraints:
	sample="[a-zA-Z0-9_]*[-][0-9]*"

##access  files  for  samples
r1 = lambda wildcards: samples.at[wildcards.sample, "fq1"]
r2 = lambda wildcards: samples.at[wildcards.sample, "fq2"]
r = lambda wildcards: samples.at[wildcards.sample, "fq" + wildcards.run]

run = ["1", "2"]

#Make sure that all final output files get created
rule make_all:
	input:
		fasttree = "fasttree.newick",
		multiqc = "report/multiqc_report.html",
		mlst = "mlst/mlst.tsv" if config["run_mlst"] else []
	shell:
		"""
		echo 'Done'
		"""


include: "rules/qc.smk"
include: "rules/assembly.smk"
include: "rules/samtools.smk"
include: "rules/phylogeny.smk"
include: "rules/mlst.smk"
